import java.util.Arrays;
import java.util.stream.Stream;

public class Kata {
  public static int[] sortArray(int[] array) {
    
    int[] sortedOdds = Arrays.stream(array).filter(i -> i % 2 != 0).sorted().toArray();
    
    int[] result = new int[array.length];
    int i = 0; // index of next possible odd element in array to replace
    
    for (int e : sortedOdds) {
      while (array[i] % 2 == 0) {
        // copy the evens
        result[i] = array[i];
        i++;
      }
      // found an odd to replace
      result[i] = e;
      i++;
    }
    
    // put the remaining evens in
    while (i < array.length) {
      result[i] = array[i];
      i++;
    }
    
    return result;
  }
}